#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

ORIGDIR=`pwd`
cd $srcdir

PROJECT=system-tray-applet
TEST_TYPE=-f
FILE=src/GNOME_SystemTrayApplet.server.in.in

DIE=0

(autoconf --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have autoconf installed to compile $PROJECT."
	echo "Download the appropriate package for your distribution,"
	echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
}

AUTOMAKE=automake-1.4
ACLOCAL=aclocal-1.4

($AUTOMAKE --version) < /dev/null > /dev/null 2>&1 || {
        AUTOMAKE=automake
        ACLOCAL=aclocal
}

($AUTOMAKE --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have automake installed to compile $PROJECT."
	echo "Get ftp://ftp.cygnus.com/pub/home/tromey/automake-1.2d.tar.gz"
	echo "(or a newer version if it is available)"
	DIE=1
}

if test "$DIE" -eq 1; then
	exit 1
fi

test $TEST_TYPE $FILE || {
	echo "You must run this script in the top-level $PROJECT directory"
	exit 1
}

if test -z "$*"; then
	echo "I am going to run ./configure with no arguments - if you wish "
        echo "to pass any to it, please specify them on the $0 command line."
fi

libtoolize --copy --force

grep "^AM_GLIB_GNU_GETTEXT" configure.in >/dev/null && {
  grep "sed.*POTFILES" $srcdir/configure.in >/dev/null || \
  (glib-gettextize --version) < /dev/null > /dev/null 2>&1 || {
    echo
    echo "**Error**: You must have \`glib' installed to compile $PROJECT."
    DIE=1
  }
}

(grep "^AC_PROG_INTLTOOL" $srcdir/configure.in >/dev/null) && {
  (intltoolize --version) < /dev/null > /dev/null 2>&1 || {
    echo
    echo "**Error**: You must have \`intltoolize' installed to compile $PKG_NAME."
    echo "Get ftp://ftp.gnome.org/pub/GNOME/stable/sources/intltool/intltool-0.10.tar.gz"
    echo "(or a newer version if it is available)"
    DIE=1
  }
}

if grep "^AM_GLIB_GNU_GETTEXT" configure.in >/dev/null; then
  if grep "sed.*POTFILES" configure.in >/dev/null; then
    : do nothing -- we still have an old unmodified configure.in
  else
    echo "Creating $dr/aclocal.m4 ..."
    test -r $dr/aclocal.m4 || touch $dr/aclocal.m4
    echo "Running glib-gettextize...  Ignore non-fatal messages."
    echo "no" | glib-gettextize --force --copy
    echo "Making $dr/aclocal.m4 writable ..."
    test -r $dr/aclocal.m4 && chmod u+w $dr/aclocal.m4
  fi
fi
if grep "^AC_PROG_INTLTOOL" configure.in >/dev/null; then
  echo "Running intltoolize..."
  intltoolize --copy --force --automake
fi

echo $ACLOCAL $ACLOCAL_FLAGS
$ACLOCAL $ACLOCAL_FLAGS

# optionally feature autoheader
(autoheader --version)  < /dev/null > /dev/null 2>&1 && autoheader

$AUTOMAKE -a $am_opt
autoconf || echo "autoconf failed - version 2.5x is probably required"

cd $ORIGDIR

$srcdir/configure --enable-maintainer-mode "$@"

echo 
echo "Now type 'make' to compile $PROJECT."
